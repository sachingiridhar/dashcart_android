package com.phacsin.dashcart.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.phacsin.dashcart.R;
import com.phacsin.dashcart.models.Store;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.MyViewHolder> {

    private Context mContext ;
    private List<Store> mData ;


    public StoreAdapter(Context mContext, List<Store> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.card_store,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.store_name.setText(mData.get(position).getTitle());
        holder.store_description.setText(mData.get(position).getDescription());
        holder.store_url.setText(mData.get(position).getUrl());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView store_url,store_name,store_description;

        public MyViewHolder(View itemView) {
            super(itemView);
            store_url =  itemView.findViewById(R.id.store_url) ;
            store_name =  itemView.findViewById(R.id.store_name);
            store_description =  itemView.findViewById(R.id.store_description);

        }
    }


}