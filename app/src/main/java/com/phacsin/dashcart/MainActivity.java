package com.phacsin.dashcart;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.button.MaterialButton;
import com.phacsin.dashcart.adapters.StoreAdapter;
import com.phacsin.dashcart.models.Store;
import com.ramotion.cardslider.CardSliderLayoutManager;
import com.ramotion.cardslider.CardSnapHelper;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {
    BottomAppBar bar;
    private CardSliderLayoutManager layoutManger;
    private StoreAdapter storeAdapter;
    RecyclerView recyclerView;
    List<Store> storeList = new ArrayList<>();
    private int currentPosition;
    MaterialButton btn_launch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bar =  findViewById(R.id.bar);
        btn_launch = findViewById(R.id.btn_launch);
        btn_launch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),LaunchStoreActivity.class));
            }
        });
        setSupportActionBar(bar);
        storeAdapter = new StoreAdapter(getApplicationContext(), storeList);
        initRecyclerView();
    }

    private void initRecyclerView() {
        recyclerView =  findViewById(R.id.recyclerview_stores);
        recyclerView.setAdapter(storeAdapter);
        recyclerView.setHasFixedSize(true);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    onActiveCardChange();
                }
            }
        });
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        layoutManger = new CardSliderLayoutManager(50,width-200,48);
        recyclerView.setLayoutManager(layoutManger);
        new CardSnapHelper().attachToRecyclerView(recyclerView);
        addStores();
    }

    private void addStores() {
        for(int i=0;i<10;i++)
        {
            Store store = new Store("Dashcart","Dashcart is a platform to take your retail store online in a jiffy. You can manage your website using a simple dashboard. You can quit using our platform with zero commitment.","dashcart.in",R.mipmap.ic_launcher);
            storeList.add(store);
        }
        Log.d("RECYC",storeList.size()+"");
        storeAdapter.notifyDataSetChanged();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void onActiveCardChange() {
        final int pos = layoutManger.getActiveCardPosition();
        if (pos == RecyclerView.NO_POSITION || pos == currentPosition) {
            return;
        }

        onActiveCardChange(pos);
    }

    private void onActiveCardChange(int pos) {

        final boolean left2right = pos < currentPosition;


        currentPosition = pos;
    }

}
